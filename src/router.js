import React from "react"
import {
  BrowserRouter as Router,
  Route,
} from "react-router-dom"
import DeploymentList from './modules/deployment/container/List'
import DeploymentCreate from './modules/deployment/container/Create'
import NavBar from './components/NavBar'

export default () => {
  return (
    <Router>
      <NavBar/>
      <Route path="/" exact component={DeploymentList}/>
      <Route path="/deployments/new" component={DeploymentCreate}/>
    </Router>
  )
}
