import React, {Component} from 'react'
import {Provider} from 'react-redux'
import store from './store'
import Router from './router'
import ModalBox from './modules/modal/container/ModalBox'
import {MuiThemeProvider, createMuiTheme} from '@material-ui/core/styles'

const theme = createMuiTheme({
  palette: {
    primary: {
      main: '#1186C5',
    },
    secondary: {
      main: '#6f82e9',
    },
  },
  typography: {
    useNextVariants: true,
  },
})

class App extends Component {
  render() {
    return (
      <Provider store={store()}>
        <MuiThemeProvider theme={theme}>
          <Router/>
          <ModalBox/>
        </MuiThemeProvider>
      </Provider>
    )
  }
}

export default App
