import {combineReducers} from 'redux'
import deployment from './modules/deployment/state/reducer'
import modal from './modules/modal/state/reducer'

export default combineReducers({
  deployment,
  modal,
})
