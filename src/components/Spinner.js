import PropTypes from 'prop-types'
import React, {Fragment} from 'react'
import CircularProgress from '@material-ui/core/CircularProgress'

function Spinner({visible, size, color, ...props}) {
  return (visible) ? <div style={{display: 'flex', justifyContent: 'center'}}><CircularProgress size={size} color={color} {...props}/></div> : <Fragment/>
}

Spinner.defaultProps = {
  visible: true,
  size: 32,
  color: "secondary",
}

Spinner.propTypes = {
  visible: PropTypes.bool,
  size: PropTypes.number,
  color: PropTypes.string,
}

export default Spinner
