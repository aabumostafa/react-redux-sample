import React from 'react'
import Button from '@material-ui/core/Button'
import {Link} from 'react-router-dom'

const RouterLink = ({to, ...props}) => <Link to={to} {...props} />
export default ({children, ...props}) => <Button component={RouterLink} {...props}>{children}</Button>
