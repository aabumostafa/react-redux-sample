import React from 'react'
import PropTypes from 'prop-types'
import Typography from '@material-ui/core/Typography'
import {withStyles} from '@material-ui/core/styles'

const styles = () => ({
  title: {
    marginBottom: 20,
  }
})

function HeaderTitle(props) {
  const {classes, title, children} = props
  return (
    <Typography variant="h5" className={classes.title}>
      {title || children}
    </Typography>
  )
}

HeaderTitle.propTypes = {
  classes: PropTypes.object.isRequired,
  title: PropTypes.string,
}

export default withStyles(styles)(HeaderTitle)
