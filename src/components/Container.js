import React from 'react'
import Paper from '@material-ui/core/Paper'
import {withStyles} from '@material-ui/core'

const styles = theme => ({
  root: {
    ...theme.mixins.gutters(),
    paddingTop: theme.spacing.unit * 2,
    paddingBottom: theme.spacing.unit * 2,
  },
})

function Container(props) {
  const {classes, children} = props
  return (
    <Paper className={classes.root}>
      {children}
    </Paper>
  )
}

export default withStyles(styles)(Container)
