import React, {Fragment} from 'react'
import PropTypes from 'prop-types'
import {withStyles} from '@material-ui/core/styles'
import Paper from '@material-ui/core/Paper'
import Typography from '@material-ui/core/Typography'

const styles = theme => ({
  root: {
    ...theme.mixins.gutters(),
    paddingTop: theme.spacing.unit * 2,
    paddingBottom: theme.spacing.unit * 2,
    backgroundColor: "#c06161",
    marginTop: theme.spacing.unit,
    marginBottom: theme.spacing.unit,
    boxShadow: 'none',
  },
  message: {
    color: theme.palette.common.white,
    fontSize: 15,
  }
})

function ErrorBox(props) {
  const {classes, visible, message} = props
  if (!visible) {
    return <Fragment/>
  }

  return (
    <div>
      <Paper className={classes.root} elevation={1}>
        {message && <Typography className={classes.message}>{message || 'this is my message to you'}</Typography>}
      </Paper>
    </div>
  )
}

ErrorBox.propTypes = {
  visible: PropTypes.bool,
  message: PropTypes.string,
  classes: PropTypes.object.isRequired,
}

export default withStyles(styles)(ErrorBox)
