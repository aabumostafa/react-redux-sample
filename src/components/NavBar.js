import React from 'react'
import PropTypes from 'prop-types'
import {withStyles} from '@material-ui/core/styles'
import AppBar from '@material-ui/core/AppBar'
import Toolbar from '@material-ui/core/Toolbar'
import MenuButton from '@material-ui/core/IconButton'
import Typography from '@material-ui/core/Typography'
import Link from './Link'
import LocalAirportIcon from '@material-ui/icons/LocalAirport'

const styles = {
  root: {
    flexGrow: 1,
  },
  grow: {
    flexGrow: 1,
  },
  menuButton: {
    marginLeft: -18,
    marginRight: 0,
  },
  navButton: {
    textTransform: 'none',
  }
}

function DenseAppBar(props) {
  const {classes} = props
  return (
    <div className={classes.root}>
      <AppBar position="static" variant="raised">
        <Toolbar>
          <MenuButton color="inherit" className={classes.menuButton}>
            <LocalAirportIcon/>
          </MenuButton>
          <Typography variant="h6" color="inherit" className={classes.grow}>
            Convox Deployer
          </Typography>
          <div style={{marginLeft: 100}}>
            <Link to="/" className={classes.navButton} color="inherit">Deployments</Link>
            <Link to="/projects" className={classes.navButton} color="inherit">Projects</Link>
            <Link to="/users" className={classes.navButton} color="inherit">Users</Link>
          </div>
        </Toolbar>
      </AppBar>
    </div>
  )
}

DenseAppBar.propTypes = {
  classes: PropTypes.object.isRequired,
}

export default withStyles(styles)(DenseAppBar)
