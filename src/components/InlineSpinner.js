import PropTypes from 'prop-types'
import React, {Fragment} from 'react'
import CircularProgress from '@material-ui/core/CircularProgress'

function InlineSpinner({visible, size, color, ...props}) {
  return (visible) ? <CircularProgress size={size} color={color} {...props}/> : <Fragment/>
}

InlineSpinner.defaultProps = {
  visible: true,
  size: 18,
  color: "secondary",
}

InlineSpinner.propTypes = {
  visible: PropTypes.bool,
  size: PropTypes.number,
  color: PropTypes.string,
}

export default InlineSpinner
