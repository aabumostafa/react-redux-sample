import {
  FETCH_DEPLOYMENTS_STARTED,
  FETCH_DEPLOYMENTS_SUCCEEDED,
  FETCH_DEPLOYMENTS_FAILED,
} from './types'

const INITIAL_STATE = {
  initialized: false,
  fetching: false,
  items: [],
  error: null,
}

export default (state = INITIAL_STATE, {type, ...payload}) => {
  switch (type) {
    case FETCH_DEPLOYMENTS_STARTED:
      return {...state, fetching: true, error: null}
    case FETCH_DEPLOYMENTS_SUCCEEDED:
      return {...state, fetching: false, initialized: true, items: payload.items, error: null}
    case FETCH_DEPLOYMENTS_FAILED:
      return {...state, fetching: false, initialized: true, items: [], error: payload.err}
    default:
      return state
  }
}
