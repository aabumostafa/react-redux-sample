import Api from '../../../../services/api'
import {
  FETCH_DEPLOYMENTS_STARTED,
  FETCH_DEPLOYMENTS_SUCCEEDED,
  FETCH_DEPLOYMENTS_FAILED,
} from './types'

export const fetchItems = () => {
  return (dispatch) => {
    dispatch({type: FETCH_DEPLOYMENTS_STARTED})
    Api
      .fetchDeployments()
      .then(items => dispatch({type: FETCH_DEPLOYMENTS_SUCCEEDED, items}))
      .catch(err => dispatch({type: FETCH_DEPLOYMENTS_FAILED, err}))
  }
}
