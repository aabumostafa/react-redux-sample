import {combineReducers} from 'redux'
import list from './list/reducer'
import create from './create/reducer'

export default combineReducers({
  list,
  create,
})
