import Api from "../../../../services/api"
import {
  FETCH_PROJECTS_STARTED,
  FETCH_PROJECTS_SUCCEEDED,
  FETCH_PROJECTS_FAILED,

  CREATE_DEPLOYMENT_STARTED,
  CREATE_DEPLOYMENT_SUCCEEDED,
  CREATE_DEPLOYMENT_FAILED,

  CREATE_DEPLOYMENT_RESET,
} from './types'

export const fetchProjects = () => {
  return (dispatch) => {
    dispatch({type: FETCH_PROJECTS_STARTED})
    Api
      .fetchProjects()
      .then(items => dispatch({type: FETCH_PROJECTS_SUCCEEDED, items}))
      .catch(err => dispatch({type: FETCH_PROJECTS_FAILED, err}))
  }
}

const validateDeployment = ({branch, project_id}) => {
  const errors = {}
  if (!project_id || project_id === '0') {
    errors["project_id"] = "Project is required"
  }

  return errors
}

export const createDeployment = (values) => {
  return (dispatch) => {
    dispatch({type: CREATE_DEPLOYMENT_STARTED})
    const formErrors = validateDeployment(values)

    if (Object.keys(formErrors).length) {
      const err = new Error('Validation Failed')
      return dispatch({type: CREATE_DEPLOYMENT_FAILED, formErrors, err})
    }

    Api
      .createDeployment(values)
      .then(deployment => dispatch({type: CREATE_DEPLOYMENT_SUCCEEDED, deployment}))
      .catch(err => {
        dispatch({
          type: CREATE_DEPLOYMENT_FAILED,
          err: new Error((err.error && err.error.message) || err.message),
          formErrors: (err.error && err.error.errors) || {},
        })
      })
  }
}

export const reset = () => {
  return (dispatch) => {
    dispatch({type: CREATE_DEPLOYMENT_RESET})
  }
}