import {
  CREATE_DEPLOYMENT_FAILED,
  CREATE_DEPLOYMENT_STARTED,
  CREATE_DEPLOYMENT_SUCCEEDED,
  CREATE_DEPLOYMENT_RESET,
  FETCH_PROJECTS_FAILED,
  FETCH_PROJECTS_STARTED,
  FETCH_PROJECTS_SUCCEEDED,
} from "./types"

const emptyProject = {
  id: 0,
  name: 'Select Project',
}

const INITIAL_STATE = {
  projects: [
    emptyProject
  ],
  loadingProjects: false,
  formErrors: {},
  error: null,
  submitting: false,
  deployment: null,
  created: false,
}

export default (state = INITIAL_STATE, {type, ...payload}) => {
  switch (type) {
    case FETCH_PROJECTS_STARTED:
      return {...state, loadingProjects: true}
    case FETCH_PROJECTS_SUCCEEDED:
      return {...state, loadingProjects: false, projects: [emptyProject, ...payload.items]}
    case FETCH_PROJECTS_FAILED:
      return {...state, loadingProjects: false, error: payload.err}

    case CREATE_DEPLOYMENT_STARTED:
      return {...state, submitting: true, errors: [], created: false}
    case CREATE_DEPLOYMENT_SUCCEEDED:
      return {...state, deployment: payload.deployment, submitting: false, formErrors: {}, created: true}
    case CREATE_DEPLOYMENT_FAILED:
      return {...state, deployment: null, submitting: false, formErrors: payload.formErrors, error: payload.err}
    case CREATE_DEPLOYMENT_RESET:
      return {...state, ...INITIAL_STATE}
    default:
      return state
  }
}
