import React, {Fragment} from 'react'
import PropTypes from 'prop-types'
import {withStyles} from '@material-ui/core/styles'
import DeploymentListItem from './DeploymentListItem'
import Table from '@material-ui/core/Table'
import TableBody from '@material-ui/core/TableBody'
import TableHead from '@material-ui/core/TableHead'
import TableRow from '@material-ui/core/TableRow'
import TableCell from '@material-ui/core/TableCell'
import Spinner from "../../../components/Spinner"

const styles = theme => ({
  root: {
    width: '100%',
  },
  headRow: {
    backgroundColor: theme.palette.secondary.main,
    color: theme.palette.common.white,
    fontSize: 14,
  }
})

const LoadingRow = ({visible}) => {
  return (visible) ? <TableRow><TableCell colSpan={8}><Spinner/></TableCell></TableRow> : <Fragment/>
}

const NoDeployments = ({visible}) => {
  return (visible) ? <TableRow><TableCell colSpan={8}>No deployments</TableCell></TableRow> : <Fragment/>
}

function DeploymentList(props) {
  const {classes, items, loading, onShowLogClick} = props
  return (
    <div className={classes.root}>
      <Table className={classes.table}>
        <TableHead>
          <TableRow>
            <TableCell className={classes.headRow}>Project</TableCell>
            <TableCell className={classes.headRow}>Branch</TableCell>
            <TableCell className={classes.headRow}>User</TableCell>
            <TableCell className={classes.headRow}>Created</TableCell>
            <TableCell className={classes.headRow}>Started</TableCell>
            <TableCell className={classes.headRow}>Finished</TableCell>
            <TableCell className={classes.headRow}>Status</TableCell>
            <TableCell className={classes.headRow}>Log</TableCell>
          </TableRow>
        </TableHead>
        <TableBody>
          <LoadingRow visible={loading}/>
          <NoDeployments visible={!loading && items.length === 0}/>
          {!loading && items.map(item => <DeploymentListItem
            key={item.id}
            item={item}
            onShowLogClick={onShowLogClick}/>
          )}
        </TableBody>
      </Table>
    </div>
  )
}

DeploymentList.propTypes = {
  classes: PropTypes.object.isRequired,
  items: PropTypes.array.isRequired,
}

export default withStyles(styles)(DeploymentList)
