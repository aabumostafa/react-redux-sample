import React, {Fragment} from 'react'
import PropTypes from 'prop-types'
import TableRow from '@material-ui/core/TableRow'
import TableCell from '@material-ui/core/TableCell'
import Chip from '@material-ui/core/Chip'
import CircularProgress from '@material-ui/core/CircularProgress'
import CheckIcon from '@material-ui/icons/Check'
import CloseIcon from '@material-ui/icons/Close'
import moment from 'moment'
import Button from '@material-ui/core/IconButton'
import ShowIcon from '@material-ui/icons/Visibility'

const renderProgressIcon = (item) => {
  if (!item.started_at) {
    return <Fragment/>
  }

  if (item.inProgress) {
    return <CircularProgress size={18} color="secondary"/>
  }

  if (item.succeeded) {
    return <CheckIcon/>
  }

  return <CloseIcon/>
}

function DeploymentListItem(props) {
  const {item, onShowLogClick} = props
  return (
    <TableRow>
      <TableCell>
        {item.project.name}
      </TableCell>
      <TableCell>
        <Chip label={item.branch}/>
      </TableCell>
      <TableCell>
        {item.user.name}
      </TableCell>
      <TableCell>
        {item.created_at && moment(item.created_at).fromNow()}
      </TableCell>
      <TableCell>
        {item.started_at && moment(item.started_at).fromNow()}
      </TableCell>
      <TableCell>
        {item.finished_at && moment(item.finished_at).fromNow()}
      </TableCell>
      <TableCell>
        {renderProgressIcon(item)}
      </TableCell>
      <TableCell>
        <Button
          onClick={() => onShowLogClick(item)}
          color="primary"
          size="small"
          variant="contained"
          disabled={!item.log}
        >
          <ShowIcon/>
        </Button>
      </TableCell>
    </TableRow>
  )
}

DeploymentListItem.propTypes = {
  item: PropTypes.object.isRequired,
}

export default DeploymentListItem
