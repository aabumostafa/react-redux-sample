import React from 'react'
import PropTypes from 'prop-types'
import {withStyles} from '@material-ui/core/styles'
import FormControl from '@material-ui/core/FormControl'
import FormHelperText from '@material-ui/core/FormHelperText'
import Input from '@material-ui/core/Input'
import InputLabel from '@material-ui/core/InputLabel'
import TextField from '@material-ui/core/TextField'
import LaunchIcon from '@material-ui/icons/AirplanemodeActive'
import Button from '@material-ui/core/Button'
import InlineSpinner from '../../../components/InlineSpinner'

const styles = theme => ({
  container: {
    display: 'flex',
    flexDirection: 'column',
    width: '50%',
  },
  formControl: {
    margin: theme.spacing.unit,
  },
  menu: {
    width: 200,
  },
  submitButton: {
    maxWidth: 150,
  },
  icon: {
    marginRight: theme.spacing.unit,
    fontSize: 20,
  },
})

class CreateDeploymentForm extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      project_id: 0,
      branch: '',
    }
  }

  onSelectProject(e) {
    this.setState({project_id: e.target.value})
  }

  onChangeBranch(e) {
    this.setState({branch: e.target.value})
  }

  renderErrorText(input) {
    const {errors} = this.props
    if (errors && errors[input]) {
      return <FormHelperText>{errors[input]}</FormHelperText>
    }
  }

  renderInputBranch() {
    const {classes, errors} = this.props
    return (
      <FormControl
        className={classes.formControl}
        error={!!(errors && errors["branch"])}
        aria-describedby="deployment-branch-text"
      >
        <InputLabel htmlFor="deployment-branch">Branch</InputLabel>
        <Input
          id="deployment-branch"
          value={this.state.branch}
          placeholder={`default "master"`}
          onChange={this.onChangeBranch.bind(this)}
        />
        {this.renderErrorText("branch")}
      </FormControl>
    )
  }

  renderInoutProject() {
    const {classes, loadingProjects, projects, errors} = this.props
    const hasError = !!(errors && errors["project_id"])
    return (
      <FormControl className={classes.formControl} error aria-describedby="component-error-text">
        <TextField
          id="deployment-project_id"
          select
          label="Project"
          value={this.state.project_id}
          onChange={this.onSelectProject.bind(this)}
          SelectProps={{
            native: true,
            MenuProps: {
              className: classes.menu,
            },
          }}
          helperText={hasError ? errors["project_id"] : "Select Project to deploy"}
          margin="normal"
          disabled={loadingProjects}
          InputProps={{
            endAdornment: <InlineSpinner visible={loadingProjects}/>,
            error: hasError,
          }}
        >
          {projects.map(project => (
            <option key={project.id} value={project.id}>
              {project.name}
            </option>
          ))}
        </TextField>
      </FormControl>
    )
  }

  renderInputSubmit() {
    const {classes, errors, submitting} = this.props
    return (
      <FormControl
        className={classes.formControl}
        error={errors && errors["branch"]}
        aria-describedby="deployment-branch-text"
      >
        <Button
          variant="contained"
          color="primary"
          className={classes.submitButton}
          size="small"
          onClick={this.onSubmit.bind(this)}
          disabled={submitting}
        >
          <LaunchIcon className={classes.icon}/>
          Deploy
        </Button>
      </FormControl>
    )
  }

  onSubmit() {
    this.props.onSubmit({
      project_id: this.state.project_id,
      branch: this.state.branch,
    })
  }

  render() {
    const {classes} = this.props
    return (
      <div className={classes.container}>
        {this.renderInoutProject()}
        {this.renderInputBranch()}
        {this.renderInputSubmit()}
      </div>
    )
  }
}

CreateDeploymentForm.propTypes = {
  classes: PropTypes.object.isRequired,
}

export default withStyles(styles)(CreateDeploymentForm)
