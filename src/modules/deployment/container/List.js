import React from 'react'
import {connect} from 'react-redux'
import {bindActionCreators} from 'redux'
import * as actions from '../state/list/actions'
import * as modalActions from '../../modal/state/actions'
import {withStyles} from '@material-ui/core/styles'
import DeploymentList from '../component/DeploymentList'
import AddIcon from '@material-ui/icons/Add'
import HeaderTitle from "../../../components/HeaderTitle"
import Link from '../../../components/Link'
import Container from '../../../components/Container'
import ErrorBox from '../../../components/ErrorBox'

const styles = () => ({
  headerWrapper: {
    display: 'flex',
    justifyContent: 'space-between',
  },
})

class List extends React.Component {
  componentDidMount() {
    this.props.actions.fetchItems()
  }

  handleShowLogClick(item) {
    const {modalActions} = this.props
    modalActions.open(item.log)
  }

  render() {
    const {state: {items, fetching, error}, classes} = this.props

    return (
      <Container>
        <div className={classes.headerWrapper}>
          <HeaderTitle title="Deployments"/>
          <Link to="/deployments/new" mini variant="fab" color="primary">
            <AddIcon/>
          </Link>
        </div>
        <ErrorBox visible={!!error} message={error && error.message}/>
        <DeploymentList items={items} loading={fetching} onShowLogClick={this.handleShowLogClick.bind(this)}/>
      </Container>
    )
  }
}

const mapActionsToProps = dispatch => {
  return {
    actions: bindActionCreators(actions, dispatch),
    modalActions: bindActionCreators(modalActions, dispatch),
  }
}

const mapStateToProps = ({deployment: {list: state}}) => {
  return {state}
}

export default withStyles(styles)(connect(mapStateToProps, mapActionsToProps)(List))
