import React from 'react'
import {connect} from 'react-redux'
import {Redirect} from "react-router-dom"
import {bindActionCreators} from 'redux'
import Container from '../../../components/Container'
import HeaderTitle from '../../../components/HeaderTitle'
import * as actions from '../state/create/actions'
import CreateDeploymentForm from "../component/CreateDeploymentForm"
import ErrorBox from '../../../components/ErrorBox'

class Create extends React.Component {

  componentDidMount() {
    this.props.actions.fetchProjects()
  }

  componentWillUnmount(){
    this.props.actions.reset()
  }

  onSubmit(values) {
    this.props.actions.createDeployment(values)
  }

  render() {
    const {state: {projects, loadingProjects, formErrors, submitting, created, error}} = this.props
    console.log(created)
    if (created) {
      return <Redirect to="/"/>
    }

    return (
      <Container>
        <HeaderTitle>Create Deployment</HeaderTitle>
        <ErrorBox visible={!!error} message={error && error.message}/>
        <CreateDeploymentForm
          projects={projects}
          loadingProjects={loadingProjects}
          onSubmit={this.onSubmit.bind(this)}
          errors={formErrors}
          submitting={submitting}
        />
      </Container>
    )
  }
}

const mapActionsToProps = dispatch => {
  return {actions: bindActionCreators(actions, dispatch)}
}

const mapStateToProps = ({deployment: {create: state}}) => {
  return {state}
}

export default connect(mapStateToProps, mapActionsToProps)(Create)
